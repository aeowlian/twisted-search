import axios from "axios";
// import TokenService from "@/common/token.service";
import { API_URL } from "@/common/config";
import { HTTP } from "@ionic-native/http/ngx";

// const config = {
//   headers: {
//     'Content-Type': 'application/x-www-form-urlencoded'
//   }
// }

const http = {
  http: null,
  token: null
};

const ApiService = {
  init(param) {
    if (param == "axios") {
      axios.defaults.baseURL = API_URL;
      axios.defaults.headers.common["Origin"] =
        "http://bethel.sgt.itmaranatha.org";
      axios.defaults.headers.common["Referer"] =
        "http://bethel.sgt.itmaranatha.org";
      axios.defaults.headers.common["Content-Type"] =
        "application/x-www-form-urlencoded";
      http.http = axios;
    } else {
      http.http = new HTTP();
    }
    // this.setHeader(param);
  },

  getHttpObject() {
    return http.http;
  },

  // setHeader(header, value) {
  //   // if(param == 'axios'){

  //   // }else{
  //   // http.http.setHeader(header, value);
  //   // }
  //   // axios.defaults.headers.common["Authorization"] = `Bearer ${TokenService.getToken()}`;
  //   // axios.defaults.headers.common["Access-Control-Allow-Origin"] = `*`;
  //   // axios.defaults.headers.common["Content-Type"] = `application/x-www-form-urlencoded`;
  //   // axios.defaults.headers.post["Accept"] = "application/json";
  //   console.log("header changed");
  // },

  setToken(token) {
    http.token = token;
  },

  query(resource, params) {
    return http.http.get(API_URL + resource, params).catch(error => {
      throw new Error(`ApiService ${error}`);
    });
  },

  get(resource, slug = "") {
    return http.http
      .get(
        `${API_URL}${resource}${slug}`,
        //params
        {},
        //headers
        {
          Authorization: "Bearer " + http.token
        }
      )
      .catch(error => {
        throw new Error(`ApiService ${error}`);
      });
  },

  post(resource, params) {
    if (http.token != null) {
      return http.http
        .post(
          `${API_URL}${resource}`,
          params,
          //headers
          {
            Authorization: "Bearer " + http.token
          }
        )
        .catch(error => {
          throw new Error(`ApiService ${error}`);
        });
    } else {
      return http.http.post(`${API_URL}${resource}`, params, {
        // headers: {
        //   Origin: 'http://bethel.sgt.itmaranatha.org',
        //   Referer: 'http://bethel.sgt.itmaranatha.org'
        // }
      }).catch(error => {
        throw new Error(`ApiService ${error}`);
      });
    }
  },

  update(resource, slug, params) {
    return http.http.put(`${API_URL}${resource}/${slug}`, params);
  },

  put(resource, params) {
    return http.http.put(`${API_URL}${resource}`, params);
  },

  delete(resource, slug="") {
    // return http.http.delete(API_URL + `${API_URL}${resource}/${slug}`).catch(error => {
    //   throw new Error(`ApiService ${error}`);
    // });

    return http.http
      .delete(
        `${API_URL}${resource}${slug}`,
        //params
        {},
        //headers
        {
          Authorization: "Bearer " + http.token
        }
      )
      .catch(error => {
        throw new Error(`ApiService ${error}`);
      });
  }
};

export default ApiService;

// export const ProblemsetsService = {
//   get (slug) {
//     return ApiService.get("problemsets", slug)
//   },
//   create (params) {
//     return ApiService.post('articles', {article: params})
//   },
//   update (slug, params) {
//     return ApiService.update('articles', slug, {article: params})
//   },
//   destroy (slug) {
//     return ApiService.delete(`articles/${slug}`)
//   }
// }
