import axios from "axios-observable";
import { API_URL } from "@/common/config";

const ApiService = {
  init() {
    axios.defaults.baseURL = API_URL;
  },

  setHeader() {
    // axios.defaults.headers.common['Authorization'] = `Bearer ${TokenService.getToken()}`
    axios.defaults.headers.post['Accept'] = 'application/json';
  },

  query(resource, params) {
    return axios.get(resource, params);
  },

  get(resource, slug = "", params) {
    return axios.get(`${resource}${slug}`, {params: params});
  },

  post(resource, params) {
    return axios.post(`${resource}`, params);
  },

  update(resource, slug, params) {
    return axios.put(`${resource}/${slug}`, params);
  },

  put(resource, params) {
    return axios.put(`${resource}`, params);
  },

  delete(resource) {
    return axios.delete(resource);
  }
};

export default ApiService;

// export const ProblemsetsService = {
//   get (slug) {
//     return ApiService.get("problemsets", slug)
//   },
//   create (params) {
//     return ApiService.post('articles', {article: params})
//   },
//   update (slug, params) {
//     return ApiService.update('articles', slug, {article: params})
//   },
//   destroy (slug) {
//     return ApiService.delete(`articles/${slug}`)
//   }
// }