# Twisted Search

## Before running app

This project requires Node.js and NPM.
Refer to https://nodejs.org/en/download to install Node.js and NPM according to your OS.

## Project setup

### Clone this project through git
```
git clone https://gitlab.com/aeowlian/twisted-search.git
```

### Enter cloned directory
```
cd twisted-search
```

### Install all dependencies
```
npm install
```

### Compiles, run, and hot-reloads for development
```
npm run serve
```

# Other commands

## Compiles and minifies for production
```
npm run build
```

## Run your tests
```
npm run test
```

## Lints and fixes files
```
npm run lint
```

## Run your unit tests
```
npm run test:unit
```

## Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
